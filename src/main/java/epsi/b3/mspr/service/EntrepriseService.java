package epsi.b3.mspr.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import epsi.b3.mspr.dao.EntrepriseDao;
import epsi.b3.mspr.model.Entreprise;


@Service
public class EntrepriseService {
		
	@Autowired
	private EntrepriseDao entrepriseDao;
	
	@Transactional
	public List<Entreprise> getListeEntreprise() {
		return entrepriseDao.getListeEntreprise();
	}
	
}
