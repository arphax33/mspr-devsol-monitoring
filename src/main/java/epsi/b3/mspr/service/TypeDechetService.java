package epsi.b3.mspr.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import epsi.b3.mspr.dao.TypeDechetDao;
import epsi.b3.mspr.model.TypeDechet;

public class TypeDechetService {
	
	@Autowired
	private TypeDechetDao typeDechetDao;
	
	@Transactional
	public List<TypeDechet> getListeTypeDechet() {
		return typeDechetDao.getListeTypeDechet();
	}

}
