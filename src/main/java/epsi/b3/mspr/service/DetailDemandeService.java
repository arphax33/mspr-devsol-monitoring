package epsi.b3.mspr.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epsi.b3.mspr.dao.DetailDemandeDao;
import epsi.b3.mspr.model.DetailDemande;

@Service
public class DetailDemandeService {
	
	@Autowired
	private DetailDemandeDao detailDemandeDao;
	
	@Transactional
	public List<DetailDemande> getListeDetailDemande() {
		return detailDemandeDao.getListDetailDemande();
	}

}
