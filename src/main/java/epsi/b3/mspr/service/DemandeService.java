package epsi.b3.mspr.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epsi.b3.mspr.dao.DemandeDao;
import epsi.b3.mspr.model.Demande;

@Service
public class DemandeService {

	@Autowired
	private DemandeDao demandeDao;
	
	@Transactional
	public List<Demande> getListeDemande() {
		return demandeDao.getListeDamande();
	}
	
}
