package epsi.b3.mspr.model;


import javax.persistence.*;

@Entity
@Table(name = "entreprise")
public class Entreprise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Siret")
    private int Siret;

    @Column(name = "RaisonSociale")
    private String raisonSociale;

    @Column(name = "NoRueEntr")
    private int noRueEntr;

    @Column(name = "RueEntr")
    private String rueEntr;

    @Column(name = "CpostalEntr")
    private int codePostalEntr;

    @Column(name = "VilleEntr")
    private String villeEntr;

    @Column(name = "NoTel")
    private String noTel;

    @Column(name = "Contact")
    private String contact;

    public int getSiret() {
        return Siret;
    }

    public void setSiret(int siret) {
        Siret = siret;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public int getNoRueEntr() {
        return noRueEntr;
    }

    public void setNoRueEntr(int noRueEntr) {
        this.noRueEntr = noRueEntr;
    }

    public String getRueEntr() {
        return rueEntr;
    }

    public void setRueEntr(String rueEntr) {
        this.rueEntr = rueEntr;
    }

    public int getCodePostalEntr() {
        return codePostalEntr;
    }

    public void setCodePostalEntr(int codePostalEntr) {
        this.codePostalEntr = codePostalEntr;
    }

    public String getVilleEntr() {
        return villeEntr;
    }

    public void setVilleEntr(String villeEntr) {
        this.villeEntr = villeEntr;
    }

    public String getNoTel() {
        return noTel;
    }

    public void setNoTel(String noTel) {
        this.noTel = noTel;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
