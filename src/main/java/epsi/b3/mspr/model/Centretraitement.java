package epsi.b3.mspr.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Centretraitement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "NomCentre")
	private String nomCentre;
	
	@Column(name = "RueCentre")
	private String rueCentre;
	
	@Column(name = "CpostalCentre")
	private int cpostalCentre;
	
	@Column(name = "VilleCentre")
	private String villeCentre;
	
	
	public String getNomCentre() {
		return nomCentre;
	}
	public void setNomCentre(String nomCentre) {
		this.nomCentre = nomCentre;
	}
	
	public String getRueCentre() {
		return rueCentre;
	}
	public void setRueCentre(String rueCentre) {
		this.rueCentre = rueCentre;
	}
	
	public int getCpostalCentre() {
		return cpostalCentre;
	}
	public void setCpostalCentre(int cpostalCentre) {
		this.cpostalCentre = cpostalCentre;
	}
	
	public String getVilleCentre() {
		return villeCentre;
	}
	public void setVilleCentre(String villeCentre) {
		this.villeCentre = villeCentre;
	}
	

}
