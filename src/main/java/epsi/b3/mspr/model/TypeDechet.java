package epsi.b3.mspr.model;

import javax.persistence.*;

@Entity
@Table(name = "typedechet")
public class TypeDechet {
    @Id
    @Column(name = "NoTypeDechet")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int noTypeDechet;

    @Column(name = "NomTypeDechet")
    private String nomTypeDechet;

    @Column(name = "NivDanger")
    private int nivDanger;

    public int getNoTypeDechet() {
        return noTypeDechet;
    }

    public void setNoTypeDechet(int noTypeDechet) {
        this.noTypeDechet = noTypeDechet;
    }

    public String getNomTypeDechet() {
        return nomTypeDechet;
    }

    public void setNomTypeDechet(String nomTypeDechet) {
        this.nomTypeDechet = nomTypeDechet;
    }

    public int getNivDanger() {
        return nivDanger;
    }

    public void setNivDanger(int nivDanger) {
        this.nivDanger = nivDanger;
    }
}
