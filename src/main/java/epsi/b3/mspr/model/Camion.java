package epsi.b3.mspr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Camion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	
	@Column(name = "DateAchat")
	private Date dateAchat;
	
	@Column(name = "Model")
	private String model;
	
	@Column(name = "Marque")
	private String marque;

	public Date getDate() {
		return dateAchat;
	}

	public void setDate(Date dateAchat) {
		this.dateAchat = dateAchat;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

}
