package epsi.b3.mspr.model;

import javax.persistence.*;

@Entity
@Table(name = "detaildemande")
public class DetailDemande {
    @Column(name = "QuantiteEnlevee")
    private int quantiteEnlevee;

    @Column(name = "Remarque")
    private String remarque;

    @Id
    @ManyToOne
    @Column(name = "NoDemande")
    private Demande demande;

    @Id
    @ManyToOne
    @Column(name = "NoTypeDechet")
    private TypeDechet typedechet;
}
