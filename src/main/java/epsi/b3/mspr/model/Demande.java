package epsi.b3.mspr.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "demande")
public class Demande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NoDemande")
    private int noDemande;

    @Column(name = "DateDemande")
    private Date dateDemande;

    @Column(name = "DateEnlevement")
    private Date dateEnlevement;

    @Column(name = "WebON")
    private int webOn;

    @ManyToOne
    @Column(name = "Siret")
    private Entreprise entreprise;

    @ManyToOne
    @Column(name = "NoTournee")
    private Tournee tournee;

    public int getNoDemande() {
        return noDemande;
    }

    public void setNoDemande(int noDemande) {
        this.noDemande = noDemande;
    }

    public Date getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(Date dateDemande) {
        this.dateDemande = dateDemande;
    }

    public Date getDateEnlevement() {
        return dateEnlevement;
    }

    public void setDateEnlevement(Date dateEnlevement) {
        this.dateEnlevement = dateEnlevement;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public Tournee getTournee() {
        return tournee;
    }

    public void setTournee(Tournee tournee) {
        this.tournee = tournee;
    }
}
