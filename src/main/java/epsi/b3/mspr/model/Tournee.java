package epsi.b3.mspr.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "tournee")
public class Tournee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "DateTournee")
	private Date dateTournee;
}
